<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkspacesTable extends Migration
{

    public function up() {
        Schema::create('workspaces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->text('xml');	
            $table->boolean('active');
            
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('data_structure_id')->unsigned();
            $table->foreign('data_structure_id')->references('id')->on('data_structures');

            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('workspaces');
    }
}
