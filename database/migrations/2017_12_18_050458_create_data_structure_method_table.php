<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataStructureMethodTable extends Migration {

    public function up() {
        Schema::create('data_structure_method', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('data_structure_id')->unsigned();
            $table->foreign('data_structure_id')->references('id')->on('data_structures');            

            $table->integer('method_id')->unsigned();
            $table->foreign('method_id')->references('id')->on('methods');            

            $table->text('xml');

            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('data_structure_method');
    }
}
