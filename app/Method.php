<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Method extends Model {
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function data_structures() {
        return $this->belongsToMany('App\DataStructures');
    }
}
