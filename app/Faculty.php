<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model {
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function university() {
        return $this->belongsTo('App\University');
    }

    public function careers() {
        return $this->hasMany('App\Careers');
    }
}
