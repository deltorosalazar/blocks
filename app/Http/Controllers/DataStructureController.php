<?php

namespace App\Http\Controllers;

use App\DataStructure;
use Illuminate\Http\Request;

class DataStructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataStructure  $dataStructure
     * @return \Illuminate\Http\Response
     */
    public function show(DataStructure $dataStructure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataStructure  $dataStructure
     * @return \Illuminate\Http\Response
     */
    public function edit(DataStructure $dataStructure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataStructure  $dataStructure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataStructure $dataStructure)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataStructure  $dataStructure
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataStructure $dataStructure)
    {
        //
    }
}
