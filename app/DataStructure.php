<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataStructure extends Model {
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function methods() {
        return $this->belongsToMany('App\Method');
    }
}
